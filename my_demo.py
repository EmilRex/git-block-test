import random
from datetime import timedelta

from prefect import flow, tags, task
from prefect.tasks import task_input_hash


@task(name="Always Succeeds Task")
def always_succeeds_task():
    return "foo"


@task(
    name="Depnds on AST",
)
def depends_on_ast(ast):
    if ast == "foo":
        return "fa"
    else:
        return "na?"


@task(name="Often Fails Task", retries=20)
def often_fails_task():
    """A task that benefits from task retries"""
    outcome = random.choice(["Fail", "Success", "Fail", "Fail", "Fail"])
    if outcome == "Fail":
        raise Exception("Random Choice was Failure")
    elif outcome == "Success":
        return "Success!!"


# this task keeps crashing
@task(
    name="Very Large Computation",
    cache_key_fn=task_input_hash,
    cache_expiration=timedelta(days=30),
)
def large_computation(small_int):
    """A task that benefits from"""
    print("Done large computation!")
    return small_int * 5


@task(
    name="Follows Large Computation",
)
def follows_large_computation(result_from_lc, succeed=True):
    if succeed is True:
        output = result_from_lc / 2
        return output
    else:
        raise Exception("I am bad task")


@task(
    name="Second After Large Computation",
)
def second_after_large_computation(result_from_flc):
    output = result_from_flc
    return output


@task(name="Looping Task")
def looping_task():
    print("This is looping")


@task(name="Task with Tag", tags=["Specific_Tag"])
def task_with_tag():
    """A task that is called by virtue of its tag."""
    print("This is a task with a Specific Tag")


@task(name="Always Fails Task")
def always_fails_task():
    raise Exception("I am bad task")
    

@flow(name="My Sub Flow")
def sub_flow():
    print("hi")
    always_succeeds_task()
    print("Sub Flow")


@flow(name="My Demo Flow")
def demo_flow(desired_outcome: str = "Success"):

    ast = always_succeeds_task.submit()

    depends_on_ast.submit(ast)

    sub_flow()

    often_fails_task.submit()

    task_result_0 = large_computation.submit(5)

    if desired_outcome == "Fail":
        task_result_1 = follows_large_computation.submit(task_result_0, False)

    else:
        task_result_1 = follows_large_computation.submit(task_result_0)

    second_after_large_computation.submit(task_result_1)

    if task_result_1.get_state().type != "COMPLETED":

        print("Task Get State != Completed")

        # slack_webhook_block = SlackWebhook.load('demo_slack_block')
        # slack_webhook_block.notify("Hello from Prefect! Your task failed!! :(")

    [looping_task() for i in range(2)]

    with tags("Specific_Tag"):
        task_with_tag.submit()

    if desired_outcome == "Fail":
        always_fails_task.submit()

    always_succeeds_task.submit()

    print("Done!")


if __name__ == "__main__":

    demo_flow("Success")
